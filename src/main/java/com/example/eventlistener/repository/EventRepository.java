package com.example.eventlistener.repository;

import com.example.eventlistener.repository.model.Event;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface EventRepository extends ElasticsearchRepository<Event, String> {
}