package com.example.eventlistener.repository.model;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Data
@Builder
@Document(indexName = "event")
@Accessors(chain=true)
public class Event {

    @Id
    private String id;

    private Object payload;

    @Field(type= FieldType.Date)
    private Long timestamp;
}
