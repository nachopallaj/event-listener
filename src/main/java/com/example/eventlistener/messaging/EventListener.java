package com.example.eventlistener.messaging;

import com.example.eventlistener.repository.EventRepository;
import com.example.eventlistener.repository.model.Event;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Instant;

@Component
@AllArgsConstructor
@Slf4j
public class EventListener {

    private EventRepository eventRepository;
    private ObjectMapper objectMapper;

    @RabbitListener(queuesToDeclare = @Queue(name = "${messaging.input.events-queue}", durable = "true"))
    void eventListener(Message message) {
        log.info("Received message {}", new String(message.getBody()));
        Object payload = null;
        try {
            payload = objectMapper.readValue(message.getBody(), Object.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Event event = Event.builder()
                .payload(payload)
                .timestamp(Instant.now().toEpochMilli())
                .build();
        eventRepository.save(event);
    }
}
